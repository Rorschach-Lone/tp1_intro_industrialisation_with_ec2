resource "aws_key_pair" "deployer" {
  key_name      = "deployer-key"
  public_key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC97FaY4eL2KpMVDMeKD8VS6fTgcKASzP29GmhyUApiNadB/PhjJMNPF4mIPy2rCfNo40f9GD0AwFjy9dU56i+3ep2sP9MmsZL3s4j/3zRBdRIXm9/PELUqMvTsmnHJbYcuNdHHyERWX1hkU5AC2qhXyOSvBaoHjH7GEDOcRSO4tixlWe+fArl2NsszRSMOjVzzlgtx+ZMy8Du65T4F5V/i/Fpg4kmSaSd8ihnXeMOyoXZkmxaaA6U3ex5xvVQZ6k8enZWFhuj5EOTJqJGJuj67iRfHDS3dYg6s7B36zBN9uzAON9LEpeJtiaL3RW/yikFovBCjaAAv4nonzSzihxNj6ok8jS/bm0Zeh+ZouGo217Hsupp9qOtArJoW1VTgEZacZ3jn4lxC03XyIvFPQeC1GnL8MWviQxtD3Wds5hL54tTOt8ZMBalKvf14bHPnsm0Lldwnilf1YBOEevXE3V14i657T6NYqaZ7PNCRKG6vdUVXkNlqf6Chg90/B2jM8/8= lone@lone-virtual-machine"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name        = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id        = aws_default_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    protocol    = "tcp"
    from_port   = 5000
    to_port     = 5000
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

provider "aws" {
  region        = "eu-west-2"
}

data "aws_ami" "ubuntu" {
  most_recent   = true

  filter {
    name        = "name"
    values      = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name        = "virtualization-type"
    values      = ["hvm"]
  }

  owners        = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.deployer.key_name}"

  tags = {
    Name        = "HelloWorld"
  }
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "usertable"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "id"
  range_key      = "user"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "user"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}
